import app from 'app';

try {
    app.init();
} catch (e) {
    throw new Error(e);
}
