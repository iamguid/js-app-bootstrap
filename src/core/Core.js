import Sandbox from './Sandbox';
import Dispatcher from './Dispatcher';

class Core {
    constructor() {
        this._dispatcher = new Dispatcher();
        this._registeredModules = {};
        this._registeredMiddlewares = [];
    }

    registerMiddleware(middleware) {
        this._registeredMiddlewares.push(middleware);
    }

    registerModule(moduleID, moduleClass) {
        if (this._registeredModules[moduleID]) {
            throw new Error(`Module ${moduleID} already registered in core`);
        }

        let sandbox = new Sandbox(this, moduleID);
        let instance = new moduleClass(sandbox);

        this._registeredModules[moduleID] = {
            instance: instance,
            sandbox: sandbox
        }
    }

    unregisterModule(moduleID) {
        if (!this._registeredModules[moduleID]) {
            throw new Error(`Module ${moduleID} not registered in core`);
        }

        this._registeredModules[moduleID].instance.destructor();
        this._registeredModules[moduleID] = undefined;
    }

    dispatch(payload) {
        this._registeredMiddlewares.forEach((middleware) => {
            middleware(payload);
        })

        return this._dispatcher.dispatch(payload);
    }

    register(handler) {
        return this._dispatcher.register(handler);
    }
}

export default Core;
