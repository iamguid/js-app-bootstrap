const _tokenPrefix = 'ID_';

class Dispatcher {
    constructor() {
        this._lastID = 0;
        this._handlers = {};
    }

    register(handler) {
        let token = _tokenPrefix + this._lastID++;
        this._handlers[token] = handler;
        return token;
    }

    unregister(token) {
        if (!this._handlers[token]) {
            throw new Error(`${token} does not map to a registered callback`);
        }

        delete this._handlers[token];
    }

    dispatch(payload) {
        var self = this;

        let callbacksKeys = Object.keys(this._handlers);

        let results = callbacksKeys.reduce((total, key) => {
            let handlerResult = self._handlers[key](payload);

            if (typeof handlerResult === 'object') {
                total.push(handlerResult);
            }

            return total;
        }, []);

        return results;
    }
}

export default Dispatcher
