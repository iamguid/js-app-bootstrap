class Sandbox {
    constructor(dispatcher, moduleClass) {
        this._dispatcher = dispatcher;
        this._moduleClass = moduleClass;

        this._registeredHandlers = [];
    }

    attachModule() {
    }

    detachModule() {
    }

    dispatch(target, action, data) {
        let results = this._dispatcher.dispatch({
            source: this._module.id,
            target: target,
            action: action,
            data: data || {}
        });

        return results.length > 0 ? results[0].result : undefined;
    }

    register(action, handler) {
        let filteredHandler = function(payload) {
            if (payload.target === this._moduleID && payload.action === action) {
                let handlerResult = handler(payload.source, payload.action, payload.data);

                return {
                    source: this._moduleID,
                    result: handlerResult
                }
            }

            return false;
        }

        let token = this._dispatcher.register(filteredHandler.bind(this))
        this._registeredHandlers.push(token);
    }
}

export default Sandbox;
