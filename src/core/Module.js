export default class Module {
    constructor(sandbox) {
        this.sandbox = sandbox;
    }

    destructor() {
        throw new Error(`\`destructor()\` not implimented`);
    }

    dispatch(target, action, data) {
        return this.sandbox.dispatch(target, action, data);
    }

    register(action, handler) {
        this.sandbox.register(action, handler);
    }
}
