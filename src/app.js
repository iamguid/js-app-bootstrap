import Core from 'core/Core';

import PageManager from 'modules/PageManager/PageManager';
import WebApi from 'modules/WebApi/WebApi';
import Auth from 'modules/Auth/Auth';
import Router from 'modules/Router/Router';

export default {
    init: function() {
        let core = new Core();

        core.registerMiddleware((payload) => {
            console.log(`[${payload.source} -> ${payload.target} (${payload.action})]: `, payload)
        });

        //core.registerModule(PageManager);
        core.registerModule('WebApi', WebApi);
        core.registerModule('Auth', Auth);
        core.registerModule('Router', Router);

        //core._dispatcher.dispatch({
            //source: 'App',
            //target: 'Router',
            //name: 'routeTo',
            //payload: {
                //route: '/login'
            //}
        //})


        var res = core.dispatch({
            source: 'App',
            target: 'Auth',
            action: 'login',
            data: {
                login: 'oups',
                password: 'pass'
            }
        })

        debugger;
    }
}
