import crossroads from 'crossroads';
import Definition from './Definition';

class Router {
    constructor(sandbox, config) {
        this.sandbox = sandbox;

        this.routes = {
            '/login': function() {
                this.sandbox.produce('doLogin')
            },

            '/logout': function() {
                this.sandbox.produce('doLogout')
            },

            '/home', function() {
                console.log('Home page')
            }
        }

        this.addRoutes();
    }

    destructor() {
        this.removeRoutes();
    }

    addRoutes() {
        Object.keys(this.routes).map((key) => {
            crossroads.addRoute(key, this.routes[key]);
        })
    }

    removeRoutes() {
        Object.keys(this.routes).map((key) => {
            crossroads.removeRoute(key);
        })
    }

    routeTo(path) {
        crossroads.parse(path);
    }

    routeToDefaultPage() {
        crossroads.parse('/home');
    }
}

export default {Router, Definition};
