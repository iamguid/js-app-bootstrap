import Module from 'core/Module';

class WebApi extends Module {
    constructor(sandbox) {
        super(sandbox);

        this.register('login', this.doLogin.bind(this));
        this.register('logout', this.doLogout.bind(this));
    }

    doLogin(data) {
        return new Promise((resolve, reject) => {
            setTimeout(() => {
                if (data.login == 'admin' && data.password == 'admin') {
                    resolve({
                        status: 200,
                        data: {}
                    })
                } else {
                    resolve({
                        status: 401,
                        data: {message: 'incorrect login or password'}
                    })
                }
            }, 1000)
        });
    }

    doLogout() {
        return new Promise((resolve, reject) => {
            setTimeout(() => {
                resolve({
                    status: 200,
                    data: {}
                })
            }, 1000)
        });
    }
}

export default WebApi;
