import Module from 'core/Module';

class Auth extends Module {
    constructor(sandbox) {
        super(sandbox);

        this.register('login', this.login.bind(this));
        this.register('logout', this.logout.bind(this));
        this.register('authenticate', this.authenticate.bind(this));
        this.register('isLoggedIn', this.isLoggedIn.bind(this));
    }

    login(login, password) {
        let loginPromise = this.dispatch('WebApi', 'login', {
            login: login,
            password: password
        });

        this.dispatch('stateChanged')
    }

    logout() {
        this.dispatch('WebApi', 'logout').then(() => {
            this.dispatch('PageManager', 'showLoginPage');
        });
    }
}

export default Auth;
