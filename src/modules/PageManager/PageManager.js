import Module from 'core/Module';

class PageManager extends Module {
    static getModuleName() {
        return 'PageManager';
    }

    constructor(sandbox) {
        super(sandbox);
        this.register(this.filterAction, this.handleAction.bind(this))
    }

    filterAction(action) {
        return action.target === PageManager.getModuleName();
    }

    handleAction(action) {
        switch (action.name) {
            case 'showLoginPage':
                console.log('Show login page and try to login for example')
                this.dispatch('Auth', 'login', {
                    login: 'oups',
                    password: 'pass',
                })
                break;
        }
    }
}

export default PageManager;
