var webpack = require('webpack');
var path = require('path');

var config = {
    context: __dirname,
    entry: './src/index.js',

    output: {
        path: path.join(__dirname, 'dist'),
        filename: 'app.bundle.js'
    },

    module: {
        loaders: [
            {
                test: /\.js$/,
                exclude: /(node_modules|bower_components)/,
                loader: 'babel',
                query: {
                    presets: ['es2015']
                }
            }
        ]
    },

    resolve: {
        root: path.resolve('./src')
    },

    devtool: 'source-map',
    debug: true,
    devServer: {
        contentBase: './dist',
    }

}

module.exports = config;
